var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var JoyTest = /** @class */ (function (_super) {
        __extends(JoyTest, _super);
        function JoyTest() {
            return _super.call(this) || this;
        }
        JoyTest.prototype.Joymove = function (data) {
            this.JoyText.text = "horizontal: " + data.horizontal + "\n vertical: " + data.vertical + "\n intensity: " + data.intensity;
            this.JoyType.text = "JoyType : Joymove";
        };
        JoyTest.prototype.Joyenable = function () {
            this.JoyType.text = "JoyType : Joyenable";
        };
        JoyTest.prototype.Joydisable = function () {
            this.JoyType.text = "JoyType : Joydisable";
        };
        return JoyTest;
    }(ui.JoyTestUI));
    view.JoyTest = JoyTest;
})(view || (view = {}));
//# sourceMappingURL=JoyTest.js.map