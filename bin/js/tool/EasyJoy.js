var JoyEvent;
(function (JoyEvent) {
    /**
     * 激活摇杆时
     */
    JoyEvent[JoyEvent["ENABLE"] = 1] = "ENABLE";
    /**
     * 离开摇杆时
     */
    JoyEvent[JoyEvent["DISABLE"] = 2] = "DISABLE";
    /**
     * 移动摇杆时
     */
    JoyEvent[JoyEvent["MOVE"] = 3] = "MOVE";
})(JoyEvent || (JoyEvent = {}));
/*
* 摇杆控制器
*/
var EasyJoy = /** @class */ (function () {
    function EasyJoy(joyConter, joyBackGround, joyRedis) {
        this.joyOffset = 0.5;
        this.isEnableJoy = false;
        this.joyCenter = joyConter;
        this.joyCenterSprite = this.joyCenter;
        this.joyRedis = joyRedis;
        this.joyBackGround = joyBackGround;
        this.joyBackGround.on(Laya.Event.MOUSE_DOWN, this, this.onEnableJoy);
        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onDisableJoy);
        Laya.stage.on(Laya.Event.MOUSE_OUT, this, this.onDisableJoy);
        Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMoveingJoy);
        Laya.timer.frameLoop(1, this, this.Update);
        this.JoyHandlerArray = [];
    }
    //-----------------------------------开放接口-----------------------------------------------
    /**
     * AddJoyEvent
     */
    EasyJoy.prototype.AddJoyEvent = function (caller, callback, joyType, joyEventTable) {
        if (callback == null)
            return false;
        this.JoyHandlerArray.forEach(function (element) {
            if (element.table == joyEventTable) {
                return false;
            }
        });
        var JoyHandler = Laya.Handler.create(caller, callback);
        JoyHandler.once = false;
        var opet = { callback: JoyHandler, type: joyType, table: joyEventTable };
        this.JoyHandlerArray.push(opet);
        return true;
    };
    EasyJoy.prototype.ClearJoyEventWithTable = function (joyEventTable) {
    };
    //-----------------------------------内部逻辑-----------------------------------------------
    EasyJoy.prototype.runHandler = function (handlerType, args) {
        this.JoyHandlerArray.forEach(function (element) {
            if (element.type == handlerType) {
                element.callback.runWith(args);
            }
        });
    };
    EasyJoy.prototype.onEnableJoy = function (e) {
        if (this.isEnableJoy) {
            return;
        }
        this.touchId = e.touchId;
        this.isEnableJoy = true;
        this.joyOffset = 0.5;
        //记录鼠标位置
        this.touchPostion = new Laya.Point(e.stageX, e.stageY);
        this.joyBackGround.globalToLocal(this.touchPostion);
        this.runHandler(JoyEvent.ENABLE);
    };
    EasyJoy.prototype.onMoveingJoy = function (e) {
        if (!this.isEnableJoy || this.touchId != e.touchId || this.touchId == undefined) {
            return;
        }
        //当移动时灵敏度为0.75
        this.joyOffset = 0.75;
        //记录鼠标位置
        this.touchPostion = new Laya.Point(e.stageX, e.stageY);
        this.joyBackGround.globalToLocal(this.touchPostion);
    };
    EasyJoy.prototype.onDisableJoy = function (e) {
        if (!this.isEnableJoy || this.touchId != e.touchId || this.touchId == undefined) {
            return;
        }
        this.runHandler(JoyEvent.MOVE, { horizontal: 0, vertical: 0, intensity: 0 });
        this.runHandler(JoyEvent.DISABLE);
        this.isEnableJoy = false;
    };
    EasyJoy.prototype.Update = function () {
        if (!this.isEnableJoy) {
            this.joyCenterSprite.x = Laya.MathUtil.lerp(this.joyCenterSprite.x, this.joyBackGround.width / 2.0, 0.5);
            this.joyCenterSprite.y = Laya.MathUtil.lerp(this.joyCenterSprite.y, this.joyBackGround.height / 2.0, 0.5);
            return;
        }
        //摇杆跟随
        this.moveJoy();
    };
    EasyJoy.prototype.moveJoy = function () {
        var distance = Math.sqrt(Math.pow(this.touchPostion.x - this.joyBackGround.width / 2.0, 2) + Math.pow(this.touchPostion.y - this.joyBackGround.height / 2.0, 2));
        var tpos = new Laya.Point(this.touchPostion.x - this.joyBackGround.width / 2.0, this.touchPostion.y - this.joyBackGround.height / 2.0);
        tpos.normalize();
        if (distance > this.joyRedis) {
            distance = this.joyRedis;
            tpos.x *= this.joyRedis;
            tpos.y *= this.joyRedis;
        }
        else {
            tpos.x *= distance;
            tpos.y *= distance;
        }
        this.runHandler(JoyEvent.MOVE, { horizontal: tpos.x / (this.joyRedis * 1.0), vertical: (-tpos.y / (this.joyRedis * 1.0)), intensity: distance / (this.joyRedis * 1.0) });
        tpos.x += this.joyBackGround.width / 2.0;
        tpos.y += this.joyBackGround.height / 2.0;
        // this.joyCenterSprite.pos(tpos.x , tpos.y);
        this.joyCenterSprite.x = Laya.MathUtil.lerp(this.joyCenterSprite.x, tpos.x, this.joyOffset);
        this.joyCenterSprite.y = Laya.MathUtil.lerp(this.joyCenterSprite.y, tpos.y, this.joyOffset);
    };
    return EasyJoy;
}());
//# sourceMappingURL=EasyJoy.js.map