/*
* 摇杆控制器
*/
var JoyController = /** @class */ (function () {
    function JoyController(joyConter, joyBackGround, joyRedis) {
        this.joyOffset = 0.5;
        this.isEnableJoy = false;
        this.joyCenter = joyConter;
        this.joyCenterSprite = this.joyCenter;
        this.joyRedis = joyRedis;
        this.joyBackGround = joyBackGround;
        this.joyBackGround.on(Laya.Event.MOUSE_DOWN, this, this.onEnableJoy);
        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onDisableJoy);
        Laya.stage.on(Laya.Event.MOUSE_OUT, this, this.onDisableJoy);
        Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMoveingJoy);
        Laya.timer.frameLoop(1, this, this.Update);
    }
    //-----------------------------------内部逻辑体-----------------------------------------------
    JoyController.prototype.onEnableJoy = function (e) {
        if (this.isEnableJoy) {
            return;
        }
        this.touchId = e.touchId;
        this.isEnableJoy = true;
        this.joyOffset = 0.5;
        //记录鼠标位置
        this.touchPostion = new Laya.Point(e.stageX, e.stageY);
        this.joyBackGround.globalToLocal(this.touchPostion);
    };
    JoyController.prototype.onMoveingJoy = function (e) {
        if (!this.isEnableJoy || this.touchId != e.touchId || this.touchId == undefined) {
            return;
        }
        //当移动时灵敏度为1
        this.joyOffset = 1;
        //记录鼠标位置
        this.touchPostion = new Laya.Point(e.stageX, e.stageY);
        this.joyBackGround.globalToLocal(this.touchPostion);
        // console.log("鼠标移动坐标 ：x:" + e.stageX + ",y:" + e.stageY);
    };
    JoyController.prototype.onDisableJoy = function (e) {
        if (!this.isEnableJoy || this.touchId != e.touchId || this.touchId == undefined) {
            return;
        }
        this.isEnableJoy = false;
    };
    JoyController.prototype.Update = function () {
        if (!this.isEnableJoy) {
            this.joyCenterSprite.x = Laya.MathUtil.lerp(this.joyCenterSprite.x, this.joyBackGround.width / 2.0, 0.5);
            this.joyCenterSprite.y = Laya.MathUtil.lerp(this.joyCenterSprite.y, this.joyBackGround.height / 2.0, 0.5);
            return;
        }
        var distance = Math.sqrt(Math.pow(this.touchPostion.x - this.joyBackGround.width / 2.0, 2) + Math.pow(this.touchPostion.y - this.joyBackGround.height / 2.0, 2));
        var tpos = new Laya.Point(this.touchPostion.x, this.touchPostion.y);
        if (distance > this.joyRedis) {
            tpos = new Laya.Point(tpos.x - this.joyBackGround.width / 2.0, tpos.y - this.joyBackGround.height / 2.0);
            tpos.normalize();
            tpos.x *= this.joyRedis;
            tpos.y *= this.joyRedis;
            tpos.x += this.joyBackGround.width / 2.0;
            tpos.y += this.joyBackGround.height / 2.0;
        }
        this.joyCenterSprite.x = Laya.MathUtil.lerp(this.joyCenterSprite.x, tpos.x, this.joyOffset);
        this.joyCenterSprite.y = Laya.MathUtil.lerp(this.joyCenterSprite.y, tpos.y, this.joyOffset);
    };
    return JoyController;
}());
//# sourceMappingURL=JoyController.js.map