// 程序入口
var GameMain = /** @class */ (function () {
    function GameMain() {
        Laya.init(720, 1280);
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_AUTO;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
        var res2d = [
            {
                url: "res/atlas/joy.atlas", type: Laya.Loader.ATLAS
            },
            {
                url: "joy/joy_center.png", type: Laya.Loader.IMAGE
            }
        ];
        Laya.loader.load(res2d, Laya.Handler.create(this, function () {
            var joyUI = new view.JoyTest();
            Laya.stage.addChild(joyUI);
            var joyController = new EasyJoy(joyUI.joy_center, joyUI.JoyBG, 100);
            joyController.AddJoyEvent(joyUI, joyUI.Joydisable, JoyEvent.DISABLE, "DISABLE");
            joyController.AddJoyEvent(joyUI, joyUI.Joyenable, JoyEvent.ENABLE, "ENABLE");
            joyController.AddJoyEvent(joyUI, joyUI.Joymove, JoyEvent.MOVE, "MOVE");
        }));
    }
    return GameMain;
}());
new GameMain();
//# sourceMappingURL=LayaSample.js.map