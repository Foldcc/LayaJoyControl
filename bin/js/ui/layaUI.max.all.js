var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var View = laya.ui.View;
var Dialog = laya.ui.Dialog;
var ui;
(function (ui) {
    var JoyTestUI = /** @class */ (function (_super) {
        __extends(JoyTestUI, _super);
        function JoyTestUI() {
            return _super.call(this) || this;
        }
        JoyTestUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.JoyTestUI.uiView);
        };
        JoyTestUI.uiView = { "type": "View", "props": { "y": 640, "x": 360, "width": 720, "pivotY": 640, "pivotX": 360, "height": 1280 }, "child": [{ "type": "Image", "props": { "y": 640, "x": 360, "width": 720, "skin": "joy/bai.jpg", "sizeGrid": "1,1,1,1", "pivotY": 640, "pivotX": 360, "height": 1280 }, "child": [{ "type": "Image", "props": { "y": 900, "x": 360, "width": 300, "var": "JoyBG", "skin": "joy/joy_center.png", "pivotY": 150, "pivotX": 150, "height": 300 }, "child": [{ "type": "Image", "props": { "y": 150, "x": 150, "width": 128, "var": "joy_center", "skin": "joy/joy_bg.png", "pivotY": 60, "pivotX": 64, "height": 120 } }] }, { "type": "Label", "props": { "y": 273, "x": 184, "width": 331, "var": "JoyText", "text": "label", "height": 80, "fontSize": 32, "color": "#3990ff", "bold": true, "align": "center" } }, { "type": "Label", "props": { "y": 152, "x": 181, "width": 331, "var": "JoyType", "text": "label", "height": 80, "fontSize": 32, "color": "#3990ff", "bold": true, "align": "center" } }] }] };
        return JoyTestUI;
    }(View));
    ui.JoyTestUI = JoyTestUI;
})(ui || (ui = {}));
//# sourceMappingURL=layaUI.max.all.js.map