// 程序入口
class GameMain{
    constructor()
    {
        Laya.init(720,1280);
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_AUTO;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
        
        let res2d = [
            {
                url: "res/atlas/joy.atlas" , type : Laya.Loader.ATLAS
            },
            {
                url: "joy/joy_center.png" , type : Laya.Loader.IMAGE
            }
        ];
        Laya.loader.load(res2d , Laya.Handler.create(this , ()=>{
            let joyUI = new view.JoyTest();
            Laya.stage.addChild(joyUI);
            let joyController = new EasyJoy(joyUI.joy_center , joyUI.JoyBG , 100);
            joyController.AddJoyEvent(joyUI , joyUI.Joydisable , JoyEvent.DISABLE , "DISABLE");
            joyController.AddJoyEvent(joyUI , joyUI.Joyenable , JoyEvent.ENABLE , "ENABLE");
            joyController.AddJoyEvent(joyUI , joyUI.Joymove , JoyEvent.MOVE , "MOVE");
        }));
    }
}
new GameMain();