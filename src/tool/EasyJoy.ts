enum JoyEvent {
    /**
     * 激活摇杆时
     */
    ENABLE = 1,
    /**
     * 离开摇杆时
     */
    DISABLE = 2,
    /**
     * 移动摇杆时
     */
    MOVE = 3,
}
/*
* 摇杆控制器
*/
class EasyJoy {
    //touchID
    private touchId: number;
    //摇杆
    private joyCenter: Laya.Node;
    //摇杆背景，必须为摇杆父物体
    private joyBackGround: Laya.Node;
    //摇杆半径
    private joyRedis: number;

    private joyOffset: number = 0.5;

    private joyCenterSprite: Laya.Sprite;

    private joyCenterPos: Laya.Vector2;
    //移动目标点
    private touchPostion: Laya.Point;

    private JoyHandlerArray: { callback: Laya.Handler, type: JoyEvent, table: string }[];

    private isEnableJoy: boolean = false;


    constructor(joyConter: Laya.Node, joyBackGround: Laya.Node, joyRedis: number) {
        this.joyCenter = joyConter;
        this.joyCenterSprite = (this.joyCenter as Laya.Sprite);
        this.joyRedis = joyRedis;
        this.joyBackGround = joyBackGround;
        this.joyBackGround.on(Laya.Event.MOUSE_DOWN, this, this.onEnableJoy);
        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onDisableJoy);
        Laya.stage.on(Laya.Event.MOUSE_OUT, this, this.onDisableJoy);
        Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMoveingJoy);
        Laya.timer.frameLoop(1, this, this.Update);
        this.JoyHandlerArray = [];
    }

    //-----------------------------------开放接口-----------------------------------------------
    /**
     * AddJoyEvent
     */
    public AddJoyEvent(caller: any, callback: Function, joyType: JoyEvent, joyEventTable: string): boolean {
        if (callback == null) return false;
        this.JoyHandlerArray.forEach(element => {
            if (element.table == joyEventTable) {
                return false;
            }
        });
        let JoyHandler = Laya.Handler.create(caller, callback);
        JoyHandler.once = false;
        let opet = { callback: JoyHandler, type: joyType, table: joyEventTable };
        this.JoyHandlerArray.push(opet);
        return true;
    }

    public ClearJoyEventWithTable(joyEventTable: string) {
        for (var index = this.JoyHandlerArray.length; index >= 0; index++) {
            if(this.JoyHandlerArray[index].table == joyEventTable){
                
            }
        }
    }

    //-----------------------------------内部逻辑-----------------------------------------------

    private runHandler(handlerType: JoyEvent, args?: any) {
        this.JoyHandlerArray.forEach(element => {
            if (element.type == handlerType) {
                element.callback.runWith(args);
            }
        });
    }

    private onEnableJoy(e: Laya.Event) {
        if (this.isEnableJoy) {
            return;
        }
        this.touchId = e.touchId;
        this.isEnableJoy = true;
        this.joyOffset = 0.5;
        //记录鼠标位置
        this.touchPostion = new Laya.Point(e.stageX, e.stageY);
        (this.joyBackGround as Laya.Sprite).globalToLocal(this.touchPostion);
        this.runHandler(JoyEvent.ENABLE);
    }

    private onMoveingJoy(e: Laya.Event) {
        if (!this.isEnableJoy || this.touchId != e.touchId || this.touchId == undefined) {
            return;
        }
        //当移动时灵敏度为0.75
        this.joyOffset = 0.75;
        //记录鼠标位置
        this.touchPostion = new Laya.Point(e.stageX, e.stageY);
        (this.joyBackGround as Laya.Sprite).globalToLocal(this.touchPostion);
        
    }

    private onDisableJoy(e: Laya.Event) {
        if (!this.isEnableJoy || this.touchId != e.touchId || this.touchId == undefined) {
            return;
        }
        this.runHandler(JoyEvent.MOVE , {horizontal : 0 , vertical : 0, intensity : 0});
        this.runHandler(JoyEvent.DISABLE);
        this.isEnableJoy = false;
    }

    private Update() {
        if (!this.isEnableJoy) {
            this.joyCenterSprite.x = Laya.MathUtil.lerp(this.joyCenterSprite.x, (this.joyBackGround as Laya.Sprite).width / 2.0, 0.5);
            this.joyCenterSprite.y = Laya.MathUtil.lerp(this.joyCenterSprite.y, (this.joyBackGround as Laya.Sprite).height / 2.0, 0.5);
            return;
        }
        //摇杆跟随
        this.moveJoy();
    }

    private moveJoy(){
        let distance = Math.sqrt(Math.pow(this.touchPostion.x - (this.joyBackGround as Laya.Sprite).width / 2.0, 2) + Math.pow(this.touchPostion.y - (this.joyBackGround as Laya.Sprite).height / 2.0, 2));
        let tpos = new Laya.Point(this.touchPostion.x - (this.joyBackGround as Laya.Sprite).width / 2.0, this.touchPostion.y - (this.joyBackGround as Laya.Sprite).height / 2.0);
        tpos.normalize();
        if (distance > this.joyRedis) {
            distance = this.joyRedis;
            tpos.x *= this.joyRedis;
            tpos.y *= this.joyRedis;
        }else{
            tpos.x *= distance;
            tpos.y *= distance;
        }
        this.runHandler(JoyEvent.MOVE , {horizontal : tpos.x/(this.joyRedis*1.0) , vertical : (-tpos.y/(this.joyRedis*1.0)) , intensity : distance/(this.joyRedis*1.0)});
        tpos.x += (this.joyBackGround as Laya.Sprite).width / 2.0;
        tpos.y += (this.joyBackGround as Laya.Sprite).height / 2.0;
        //直接跟随
        // this.joyCenterSprite.pos(tpos.x , tpos.y);
        //线性跟随
        this.joyCenterSprite.x = Laya.MathUtil.lerp(this.joyCenterSprite.x, tpos.x, this.joyOffset);
        this.joyCenterSprite.y = Laya.MathUtil.lerp(this.joyCenterSprite.y, tpos.y, this.joyOffset);
    }
}

