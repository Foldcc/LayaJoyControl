/**Created by the LayaAirIDE*/
module view{
	export class JoyTest extends ui.JoyTestUI{
		constructor(){
			super();
		}

		Joymove(data : {horizontal : number , vertical : number, intensity : number}){
			this.JoyText.text = "horizontal: " + data.horizontal + "\n vertical: " + data.vertical + "\n intensity: " + data.intensity;
			this.JoyType.text = "JoyType : Joymove";
		}
		Joyenable(){
			this.JoyType.text = "JoyType : Joyenable";
		}
		Joydisable(){
			this.JoyType.text = "JoyType : Joydisable";
		}
	}
}