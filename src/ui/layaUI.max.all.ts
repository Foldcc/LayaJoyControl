
import View=laya.ui.View;
import Dialog=laya.ui.Dialog;
module ui {
    export class JoyTestUI extends View {
		public JoyBG:Laya.Image;
		public joy_center:Laya.Image;
		public JoyText:Laya.Label;
		public JoyType:Laya.Label;

        public static  uiView:any ={"type":"View","props":{"y":640,"x":360,"width":720,"pivotY":640,"pivotX":360,"height":1280},"child":[{"type":"Image","props":{"y":640,"x":360,"width":720,"skin":"joy/bai.jpg","sizeGrid":"1,1,1,1","pivotY":640,"pivotX":360,"height":1280},"child":[{"type":"Image","props":{"y":900,"x":360,"width":300,"var":"JoyBG","skin":"joy/joy_center.png","pivotY":150,"pivotX":150,"height":300},"child":[{"type":"Image","props":{"y":150,"x":150,"width":128,"var":"joy_center","skin":"joy/joy_bg.png","pivotY":60,"pivotX":64,"height":120}}]},{"type":"Label","props":{"y":273,"x":184,"width":331,"var":"JoyText","text":"label","height":80,"fontSize":32,"color":"#3990ff","bold":true,"align":"center"}},{"type":"Label","props":{"y":152,"x":181,"width":331,"var":"JoyType","text":"label","height":80,"fontSize":32,"color":"#3990ff","bold":true,"align":"center"}}]}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.JoyTestUI.uiView);

        }

    }
}
